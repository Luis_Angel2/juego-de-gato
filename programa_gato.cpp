#include <iostream>
#include <stdlib.h>
//#include <conio.h>
#include <time.h>

/* 
   Esime Zacantenco
   Programacion orientada a objetos
   Luis Angel López Alavrado 2AV6 
   27/diciembre/2020
   programa Juego del Gato
 */

using namespace std;
class juego{
	protected:
	int opcion;
	int i,j,k;
	char c[3][3];
	char aux;
	public:
		juego();
		int menu();
		void tablero();
		void intro();
};
juego::juego(){
}
int juego::menu(){
	int x=0;
	do{
		//system("clear");
		cout<<"\tEscoga el modo de juego"<<endl;
		cout<<"1.-PvP"<<endl<<"2.-PvPC"<<endl;
		cin>>opcion;
		if(opcion==1||opcion==2)
		x=1;
		else{
		cout<<"Opcion invalida, vuelva a intentar."<<endl;
		getchar();
		}
	}while(x==0);
	return opcion;
}
void juego::tablero(){
	for(i=0;i<3;i++){
		for(j=0;j<3;j++){
			cout<<"["<<c[i][j]<<"]";
		}
		cout<<endl;
	}
	cout<<endl;
}
void juego::intro(){
	aux = '1';
	for (i=0;i<3;i++){
		for (j=0;j<3;j++){
			c[i][j] = aux++;
		}
	}
}
class PvP : public juego{
	protected:
	int a,v;
	public:
		PvP();
		void jugador1(){
		do{
		do{
			cout<<"Coloca una ficha jugador 1"<<endl;
			fflush(stdin);
			cin>>aux;
		}while (aux<'1'||aux>'9');
		k=0;
		switch (aux){
			case '1':
				i=0;
				j=0;
				if(c[i][j]=='X'||c[i][j]=='O'){
					k=1;
					cout<<"Casilla ocupada, intenta con otro numero!"<<endl;
					getchar();
				}
				break;
			case '2':
				i=0;
				j=1;
				if(c[i][j]=='X'||c[i][j]=='O'){
					k=1;
					cout<<"Casilla ocupada, intenta con otro numero!"<<endl;
					getchar();
				}
				break;
			case '3':
				i=0;
				j=2;
				if(c[i][j]=='X'||c[i][j]=='O'){
					k=1;
					cout<<"Casilla ocupada, intenta con otro numero!"<<endl;
					getchar();
				}
				break;
			case '4':
				i=1;
				j=0;
				if(c[i][j]=='X'||c[i][j]=='O'){
					k=1;
					cout<<"Casilla ocupada, intenta con otro numero!"<<endl;
					getchar();
				}
				break;
			case '5':
				i=1;
				j=1;
				if(c[i][j]=='X'||c[i][j]=='O'){
					k=1;
					cout<<"Casilla ocupada, intenta con otro numero!"<<endl;
					getchar();
			}
				break;
			case '6':
				i=1;
				j=2;
				if(c[i][j]=='X'||c[i][j]=='O'){
					k=1;
					cout<<"Casilla ocupada, intenta con otro numero!"<<endl;
					getchar();
				}
				break;
			case '7':
				i=2;
				j=0;
				if(c[i][j]=='X'||c[i][j]=='O'){
					k=1;
					cout<<"Casilla ocupada, intenta con otro numero!"<<endl;
					getchar();
				}
				break;
			case '8':
				i=2;
				j=1;
				if(c[i][j]=='X'||c[i][j]=='O'){
					k=1;
					cout<<"Casilla ocupada, intenta con otro numero!"<<endl;
					getchar();
				}
				break;
			case '9':
				i=2;
				j=2;
				if(c[i][j]=='X'||c[i][j]=='O'){
					k=1;
					cout<<"Casilla ocupada, intenta con otro numero!"<<endl;
					getchar();
				}
				break;
			default:
				k=1;
				cout<<"Opcion no valida"<<endl;
				getchar();
				break;
		}
	}while (k==1);
	c[i][j] = 'X';
		}
		void jugador2(){
		do{
		do{
			cout<<"Coloca una ficha jugador 2"<<endl;
			fflush(stdin);
			cin>>aux;
		}while (aux<'1'||aux>'9');
		k=0;
		switch (aux){
			case '1':
				i=0;
				j=0;
				if(c[i][j]=='X'||c[i][j]=='O'){
					k=1;
					cout<<"Casilla ocupada, intenta con otro numero!"<<endl;
					getchar();
				}
				break;
			case '2':
				i=0;
				j=1;
				if(c[i][j]=='X'||c[i][j]=='O'){
				k=1;
					cout<<"Casilla ocupada, intenta con otro numero!"<<endl;
					getchar();
				}
				break;
			case '3':
				i=0;
				j=2;
				if(c[i][j]=='X'||c[i][j]=='O'){
					k=1;
					cout<<"Casilla ocupada, intenta con otro numero!"<<endl;
					getchar();
				}
				break;
			case '4':
				i=1;
				j=0;
				if(c[i][j]=='X'||c[i][j]=='O'){
					k=1;
					cout<<"Casilla ocupada, intenta con otro numero!"<<endl;
					getchar();
				}
				break;
			case '5':
				i=1;
				j=1;
				if(c[i][j]=='X'||c[i][j]=='O'){
					k=1;
					cout<<"Casilla ocupada, intenta con otro numero!"<<endl;
					getchar();
				}
				break;
			case '6':
				i=1;
				j=2;
				if(c[i][j]=='X'||c[i][j]=='O'){
					k=1;
					cout<<"Casilla ocupada, intenta con otro numero!"<<endl;
					getchar();
				}
				break;
			case '7':
				i=2;
				j=0;
				if(c[i][j]=='X'||c[i][j]=='O'){
					k=1;
					cout<<"Casilla ocupada, intenta con otro numero!"<<endl;
					getchar();
					}
				break;
			case '8':
				i=2;
				j=1;
				if(c[i][j]=='X'||c[i][j]=='O'){
					k=1;
					cout<<"Casilla ocupada, intenta con otro numero!"<<endl;
					getchar();
				}
				break;
			case '9':
				i=2;
				j=2;
				if(c[i][j]=='X'||c[i][j]=='O'){
					k=1;
					cout<<"Casilla ocupada, intenta con otro numero!"<<endl;
					getchar();
				}

				break;
			default:
				cout<<"Opcion no valida"<<endl;
				getchar();
				break;
		}
	}while (k==1);
	c[i][j] = 'O';
		}
		void partida(){
			juego::intro();
			do{
		//system("clear");
		juego::tablero();
		if(a%2==0){
			jugador1();
		}
		else{
			jugador2();
		}
		v = victoria();
		a++;
		}while(a<=9&&v==2);
		//system("clear");
		juego::tablero();
		if(v==0)
		cout<<"Felicidades jugador 1, has ganado!!"<<endl;
		else
		cout<<"Felicidades jugador 2, has ganado!!"<<endl;
	}
		int victoria(){
		if(c[0][0]=='X'||c[0][0]=='O'){
			if(c[0][0]==c[0][1]&&c[0][0]==c[0][2]){
				if(c[0][0]=='X')
			return 0;//jugador 1
			else
			return 1;//jugador 2
			}
			if(c[0][0]==c[1][0]&&c[0][0]==c[2][0]){
				if(c[0][0]=='X')
			return 0;//jugador 1
			else
			return 1;//jugador 2
			}
		}
		if(c[1][1]=='X'||c[1][1]=='O'){
			if(c[1][1]==c[0][0]&&c[1][1]==c[2][2]){
				if(c[1][1]=='X')
			return 0;//jugador 1
			else
			return 1;//jugador 2
			}
			if(c[1][1]==c[1][0]&&c[1][1]==c[1][2]){
				if(c[1][1]=='X')
			return 0;//jugador 1
			else
			return 1;//jugador 2
			}
			if(c[1][1]==c[2][0]&&c[1][1]==c[0][2]){
				if(c[1][1]=='X')
			return 0;//jugador 1
			else
			return 1;//jugador 2
			}
			if(c[1][1]==c[0][1]&&c[1][1]==c[2][1]){
				if(c[1][1]=='X')
			return 0;//jugador 1
			else
			return 1;//jugador 2
			}
		}
		if(c[2][2]=='X'||c[2][2]=='O'){
			if(c[2][2]==c[2][0]&&c[2][2]==c[2][1]){
				if(c[1][1]=='X')
			return 0;//jugador 1
			else
			return 1;//jugador 2
			}
			if(c[2][2]==c[0][2]&&c[2][2]==c[1][2]){
				if(c[1][1]=='X')
			return 0;//jugador 1
			else
			return 1;//jugador 2
			}
		}
		return 2;
	}
};
PvP::PvP(){
}
class PvPC : public juego{
	protected:
	int a,v;
	public:
		PvPC();
		void jugador1(){
		do{
		do{
			cout<<"Coloca una ficha jugador 1"<<endl;
			fflush(stdin);
			cin>>aux;
		}while (aux<'1'||aux>'9');
		k=0;
		switch (aux){
			case '1':
				i=0;
				j=0;
				if(c[i][j]=='X'||c[i][j]=='O'){
					k=1;
					cout<<"Casilla ocupada, intenta con otro numero!"<<endl;
					getchar();
				}
				break;
			case '2':
				i=0;
				j=1;
				if(c[i][j]=='X'||c[i][j]=='O'){
					k=1;
					cout<<"Casilla ocupada, intenta con otro numero!"<<endl;
					getchar();
				}
				break;
			case '3':
				i=0;
				j=2;
				if(c[i][j]=='X'||c[i][j]=='O'){
					k=1;
					cout<<"Casilla ocupada, intenta con otro numero!"<<endl;
					getchar();
				}
				break;
			case '4':
				i=1;
				j=0;
				if(c[i][j]=='X'||c[i][j]=='O'){
					k=1;
					cout<<"Casilla ocupada, intenta con otro numero!"<<endl;
					getchar();
				}
				break;
			case '5':
				i=1;
				j=1;
				if(c[i][j]=='X'||c[i][j]=='O'){
					k=1;
					cout<<"Casilla ocupada, intenta con otro numero!"<<endl;
					getchar();
			}
				break;
			case '6':
				i=1;
				j=2;
				if(c[i][j]=='X'||c[i][j]=='O'){
					k=1;
					cout<<"Casilla ocupada, intenta con otro numero!"<<endl;
					getchar();
				}
				break;
			case '7':
				i=2;
				j=0;
				if(c[i][j]=='X'||c[i][j]=='O'){
					k=1;
					cout<<"Casilla ocupada, intenta con otro numero!"<<endl;
					getchar();
				}
				break;
			case '8':
				i=2;
				j=1;
				if(c[i][j]=='X'||c[i][j]=='O'){
					k=1;
					cout<<"Casilla ocupada, intenta con otro numero!"<<endl;
					getchar();
				}
				break;
			case '9':
				i=2;
				j=2;
				if(c[i][j]=='X'||c[i][j]=='O'){
					k=1;
					cout<<"Casilla ocupada, intenta con otro numero!"<<endl;
					getchar();
				}
				break;
			default:
				k=1;
				cout<<"Opcion no valida"<<endl;
				getchar();
				break;
		}
	}while (k==1);
	c[i][j] = 'X';
		}
		void jugador2(){
			srand(time(NULL));
		do{
			i=rand()%3;
			j=rand()%3;
			k=0;
			if(c[i][j]=='X'||c[i][j]=='O')
			k=1;
	}while (k==1);
	c[i][j] = 'O';
		}
		void partida(){
			juego::intro();
			do{
		//system("clear");
		juego::tablero();
		if(a%2==0){
			jugador1();
		}
		else{
			jugador2();
		}
		v = victoria();
		a++;
		}while(a<=9&&v==2);
		//system("clear");
		juego::tablero();
		if(v==0)
		cout<<"Felicidades jugador 1, has ganado!!"<<endl;
		else
		cout<<"Felicidades jugador 2, has ganado!!"<<endl;
	}
		int victoria(){
		if(c[0][0]=='X'||c[0][0]=='O'){
			if(c[0][0]==c[0][1]&&c[0][0]==c[0][2]){
				if(c[0][0]=='X')
			return 0;//jugador 1
			else
			return 1;//jugador 2
			}
			if(c[0][0]==c[1][0]&&c[0][0]==c[2][0]){
				if(c[0][0]=='X')
			return 0;//jugador 1
			else
			return 1;//jugador 2
			}
		}
		if(c[1][1]=='X'||c[1][1]=='O'){
			if(c[1][1]==c[0][0]&&c[1][1]==c[2][2]){
				if(c[1][1]=='X')
			return 0;//jugador 1
			else
			return 1;//jugador 2
			}
			if(c[1][1]==c[1][0]&&c[1][1]==c[1][2]){
				if(c[1][1]=='X')
			return 0;//jugador 1
			else
			return 1;//jugador 2
			}
			if(c[1][1]==c[2][0]&&c[1][1]==c[0][2]){
				if(c[1][1]=='X')
			return 0;//jugador 1
			else
			return 1;//jugador 2
			}
			if(c[1][1]==c[0][1]&&c[1][1]==c[2][1]){
				if(c[1][1]=='X')
			return 0;//jugador 1
			else
			return 1;//jugador 2
			}
		}
		if(c[2][2]=='X'||c[2][2]=='O'){
			if(c[2][2]==c[2][0]&&c[2][2]==c[2][1]){
				if(c[1][1]=='X')
			return 0;//jugador 1
			else
			return 1;//jugador 2
			}
			if(c[2][2]==c[0][2]&&c[2][2]==c[1][2]){
				if(c[1][1]=='X')
			return 0;//jugador 1
			else
			return 1;//jugador 2
			}
		}
		return 2;
	}
};
PvPC::PvPC(){
}
int main(){
	PvP p1;
	PvPC p2;
	int op;
	cout<<"\t\tBienvenido al juego de gato!!"<<endl<<endl;
	cout<<"Presione cualquier tecla para elegir un modo de juego...";
	getchar();
	op=p1.menu();
	if(op==1){
		p1.partida();
	}
	else{
		p2.partida();
	}
}
